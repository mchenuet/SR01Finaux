#include <tout_ce_qu_il_faut.h>
#define size 5

char image[size][size];
pid_t p_lecteur, p_redacteur;
int fd, piped[2], status, i,j;

int main(){

  pipe(piped);  // 1
  p_lecteur = fork(); // 2

  if(!p_lecteur){

    fd = open("toto.img", O_RDWR, 0666);  // 3
    read(fd, image, size * size * sizeof(char));  // 4
    close(fd);  // 5

    close(piped[0]);

    for(i=0;i<size;i++)
     for(j=0;j<size;j++)
      write(piped[1], image[j][i], sizeof(char)); // 6

    close(piped[1]);
    _exit(0);

  }else{

    p_redacteur = fork(); // 7

    if(!p_redacteur){

       waitpid(p_lecteur, &status, 0);  // 8
       close(piped[1]);
       fd=open("toto.img",O_RDWR, 0666);

       for(i=0;i<size;i++){
         for(j=0;j<size;j++){
           read(piped[0], image[i][j], sizeof(char)); // 9
           write(fd, image[i][j], sizeof(char)); // 10
         }
       }

       close(piped[0]); close(fd);
       _exit(0);

    } else while(wait(&status)!=-1);

}
