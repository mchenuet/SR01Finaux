from tkinter import *

def bonjour(event):
    global N
    N = N + 1
    label.configure(text = "Bonjour " + str(N) + " fois")

N = 0

fenetre = Tk()
bouton = Button(fenetre, text="Bonjour")
bouton.pack()
bouton.bind("<Button-1>", bonjour)

label = Label(fenetre, text = "Bonjour 0 fois")
label.pack()
fenetre.mainloop()
