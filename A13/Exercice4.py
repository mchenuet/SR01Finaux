from tkinter import *

def bonjour(event) :
    #-----(6)------
    global N
    #-----(7)------
    N += 1
    #-----(8)------
    label.configure(text = "Bonjour " + str(N) + " fois")

N=0;

#-----(1)-----
fenetre = Tk()

bouton=Button(fenetre, text='Bonjour')
#-----(2)-----
bouton.pack()
#-----(3)-----
bouton.bind("<1>", bonjour)

label=Label(fenetre, text= 'Bonjour 0 fois')
#-----(4)-----
label.pack()
#-----(5)-----
fenetre.mainloop()
